## require File.expand_path(File.dirname(__FILE__) + '/neo')

# Greed is a dice game where you roll up to five dice to accumulate
# points.  The following "score" function will be used to calculate the
# score of a single roll of the dice.
#
# A greed roll is scored as follows:
#
# * A set of three ones is 1000 points
#
# * A set of three numbers (other than ones) is worth 100 times the
#   number. (e.g. three fives is 500 points).
#
# * A one (that is not part of a set of three) is worth 100 points.
#
# * A five (that is not part of a set of three) is worth 50 points.
#
# * Everything else is worth 0 points.
#
#
# Examples:
#
# score([1,1,1,5,1]) => 1150 points
# score([2,3,4,6,2]) => 0 points
# score([3,4,5,3,3]) => 350 points
# score([1,5,1,2,4]) => 250 points
#
# More scoring examples are given in the tests below:
#
# Your goal is to write the score method.

def score(dice)
  return 0 if dice == []

  score = 0
  occurrence = {}

  (1..6).each do |face|
    count = dice.count(face)
    occurrence[face] = count unless count == 0
  end

  occurrence.each do |face, times|
    if times >= 3
      if face == 1
        score += 1000
      else
        score += face * 100
      end
      times = times - 3
    end

    case face
    when 1 
      score += times * 100
    when 5 
      score += times * 50
    end
  end

  puts score
  return score
end

score([])
score([5])
score([1])
score([1,5,5,1])
score([2,3,4,6])
score([1,1,1])
score([2,2,2])
score([3,3,3])
score([4,4,4])
score([5,5,5])
score([6,6,6])
score([2,5,2,2,3])
score([5,5,5,5])
score([1,1,1,1])
score([1,1,1,1,1])
score([1,1,1,5,1])